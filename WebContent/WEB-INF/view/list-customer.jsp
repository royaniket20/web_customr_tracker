<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 <%@ page isELIgnored="false"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"> --%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

<title>Customer List</title>
</head>
<body>
<div class="alert alert-secondary" role="alert">
 <h1>CRM - Customer Relationship Manager</h1>
</div>
<!-- 
 -->
<div class="container-fluid">
<div class="row">
<div class="col-2 mb-2">
<button type="button" class="btn btn-primary" onclick="window.location.href='showFormForAdd'">Add Customer</button>

</div>
<div class="col-10 mb-2">

<form:form action="searchCustomer" method="POST">
<div class="input-group mb-2">
  <input type="text" name ="searchString" class="form-control" placeholder="Type User Name" aria-label="Recipient's username" aria-describedby="basic-addon2">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="submit">Search</button>
  </div>
  </div>
  </form:form>

</div>
<div class="col-12">
<div class="alert alert-primary" role="alert">
List of customers
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Email Id</th>
       <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var ="tempCustomer" items="${customers}" >
   <c:url var="updateLink" value="/customer/showFormForUpdate">
   <c:param name="customerId" value="${tempCustomer.id }"></c:param>
   </c:url>
   <c:url var="deleteLink" value="/customer/deleteCustomer">
   <c:param name="customerId" value="${tempCustomer.id }"></c:param>
   </c:url>
  <tr>
    <td>${tempCustomer.id}</td>
       <td>${tempCustomer.firstName}</td>
      <td>${tempCustomer.lastName}</td>
       <td>${tempCustomer.email}</td>
        <td><a href="${updateLink}">Update</a> | <a href="${deleteLink}" onclick="return confirm('Are you sure?')">Delete</a></td>
    </tr>
    </c:forEach>
  

  </tbody>
</table>
</div>
</div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>