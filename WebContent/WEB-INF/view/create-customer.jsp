<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

<title>create customer</title>
</head>
<body>
<div class="alert alert-secondary" role="alert">
 <h1>CRM - Customer Relationship Manager</h1>
</div>

<div class="container-fluid">
<div class="row">
<div class="col-12">
<div class="alert alert-primary" role="alert">
Add /Update Customer
</div>
<div class="card">
  <div class="card-header">
    Enter Customer Details
  </div>
  <div class="card-body">
  
   <form:form action="${pageContext.request.contextPath }/customer/saveCustomer" modelAttribute="customer" method="POST">
 <form:hidden path="id"/>
  <div class="form-group">
    <label for="exampleInputEmail1">First Name</label>
    <form:input type="text" class="form-control" id="exampleInputEmail1" path="firstName" aria-describedby="emailHelp" placeholder="Enter First Name"/>
</div>
 <div class="form-group">
    <label for="exampleInputEmail2">Last Name</label>
    <form:input type="text" class="form-control"  id="exampleInputEmail2" path="lastName" aria-describedby="emailHelp" placeholder="Enter Last Name"/>
</div>
 <div class="form-group">
    <label for="exampleInputEmail3">Email address</label>
    <form:input type="text" class="form-control"  id="exampleInputEmail3" path="email" aria-describedby="emailHelp" placeholder="Enter email address"/>
</div>
  <button type="submit" class="btn btn-primary">Submit</button>
    <button type="button" onclick="window.location.href='${pageContext.request.contextPath }/customer/list'" class="btn btn-primary">Back To List</button>
</form:form>
    
   
  </div>
</div>
</div>
</div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>