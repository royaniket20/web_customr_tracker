--First drop the unnecessary database schema - so now this user will not be able to create any db object as it dows not have any schema attached to it as the default schema public is deleted

-- create a user with login password 
drop schema springstudent cascade;
drop user springstudent;
CREATE USER springstudent WITH PASSWORD 'springstudent';

commit;
-- now create schema for the user
CREATE SCHEMA AUTHORIZATION springstudent; -- but this will not work for rds due to role issue
-- so there is a fix for that as rds we dont have full admin access
GRANT springstudent TO postgres;-- now root user is part of this role
--now below code will work
CREATE SCHEMA AUTHORIZATION springstudent; 


-------------------start of database---------------------
drop table if exists  customer cascade;

Create table customer(
id serial primary key,
firstName varchar(20),
lastName varchar(20),
email varchar(40)
);
insert into customer(firstName,lastName,email) values('Aniket','Roy','royaniket20@gmail.com');
insert into customer(firstName,lastName,email) values('Amit','Roy','royamit20@gmail.com');
insert into customer(firstName,lastName,email) values('Aloke','Roy','royaloke20@gmail.com');



 