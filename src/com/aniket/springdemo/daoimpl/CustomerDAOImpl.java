package com.aniket.springdemo.daoimpl;

import java.util.List;


import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aniket.springdemo.dao.CustomerDAO;
import com.aniket.springdemo.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Customer> getCustomers() {
		Session session = sessionFactory.getCurrentSession();
		
		Query<Customer> query = session.createQuery("from Customer order by lastName",Customer.class);
		List<Customer> customers = query.getResultList();
		
		return customers;
	}

	@Override
	public void saveCustomer(Customer customer) {
		Session session = sessionFactory.getCurrentSession();
		//session.save(customer);
		session.saveOrUpdate(customer);
		
	}

	@Override
	public Customer getCustomer(int id) {
		Session session = sessionFactory.getCurrentSession();
		Customer customer = session.get(Customer.class, id);
		return customer;
	}

	@Override
	public void deleteCustomer(int id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("delete from Customer where id=:customerID");
		query.setParameter("customerID", id);
		query.executeUpdate();
	}

	@Override
	public List<Customer> getSearchedCustomer(String searchString) {
	Session session = sessionFactory.getCurrentSession();
	Query<Customer> query = null;
		if(searchString!=null && searchString.length()>0) {
			 query = session.createQuery("from Customer where lower(firstName) like :searchString or lower(lastName) like : searchString order by lastName",Customer.class);
             query.setParameter("searchString", "%"+searchString.toLowerCase()+"%"); 		
		}else {
			 query = session.createQuery("from Customer order by lastName",Customer.class);
		}
		
		List<Customer> customers = query.getResultList();
		
		return customers;
	}

}
