package com.aniket.springdemo.serviceimpl;

import java.util.List;


import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aniket.springdemo.dao.CustomerDAO;
import com.aniket.springdemo.entity.Customer;
import com.aniket.springdemo.service.CustomerService;

@Repository
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDAO;
	
	@Transactional
	@Override
	public List<Customer> getCustomers() {
		
		return customerDAO.getCustomers();
	}
	@Transactional
	@Override
	public void saveCustomer(Customer customer) {
	 customerDAO.saveCustomer(customer);
		
	}
	@Transactional
	@Override
	public Customer getCustomer(int id) {
		
		return customerDAO.getCustomer(id);
	}
	@Override
	@Transactional
	public void deleteCustomer(int id) {
		customerDAO.deleteCustomer(id);
	}
	@Override
	@Transactional
	public List<Customer> getSearchedCustomers(String searchString) {
	
		return customerDAO.getSearchedCustomer(searchString);
	}

}
