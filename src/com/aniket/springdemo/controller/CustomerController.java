package com.aniket.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aniket.springdemo.dao.CustomerDAO;
import com.aniket.springdemo.entity.Customer;
import com.aniket.springdemo.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

@Autowired
private CustomerService customerService;

	//@RequestMapping(path="/list",method=RequestMethod.GET)
    @GetMapping("/list")	
    public String listCustomer(Model model)
	{
		List<Customer> customers = customerService.getCustomers();
		model.addAttribute("customers",customers);
		return "list-customer";
	}
    @GetMapping("/showFormForAdd")
    public String showCustomerForm(Model model) {
    	Customer  customer = new Customer();
    	model.addAttribute("customer",customer);
    	return "create-customer";
    }
    @PostMapping("/saveCustomer")
    public String saveCustomer(@ModelAttribute("customer") Customer customer) {
    	System.out.println(customer.toString());
    	customerService.saveCustomer(customer);
      return "redirect:/customer/list" ;
    }
    
    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("customerId") int id,Model model)
    {
    	Customer  customer = customerService.getCustomer(id);
    	model.addAttribute("customer",customer);
    	return "create-customer";
    }
    @GetMapping("/deleteCustomer")
    public String deleteCustomer(@RequestParam("customerId") int id)
    {
         customerService.deleteCustomer(id);
    	  return "redirect:/customer/list" ;
    }
    @PostMapping("/searchCustomer")
    public String listSearchedCustomer(Model model,@RequestParam("searchString") String searchString)
    {
    	List<Customer> customers = customerService.getSearchedCustomers(searchString);
		model.addAttribute("customers",customers);
		return "list-customer";
    }
}
