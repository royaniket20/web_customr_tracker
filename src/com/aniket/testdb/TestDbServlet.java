package com.aniket.testdb;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestDbServlet
 */
@WebServlet("/TestDbServlet")
public class TestDbServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public TestDbServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String user ="springstudent";
	String password ="springstudent";
	String jdbcurl = "jdbc:postgresql://localhost:5432/postgres";
	String driver = "org.postgresql.Driver";
	
	try {
		PrintWriter out = response.getWriter();
		Class.forName(driver);
		out.println("===connecting to the database "+jdbcurl);
		Connection connection = DriverManager.getConnection(jdbcurl, user, password);
	    out.println("isconnectionclosed = "+connection.isClosed());
		connection.close();
	} catch (Exception e) {
		e.printStackTrace();
	throw new ServletException(e);
	}
	
	}

}
